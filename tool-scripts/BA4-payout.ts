import {
  AnchorMode,
  bufferCV,
  bufferCVFromString,
  callReadOnlyFunction,
  ClarityType,
  contractPrincipalCV,
  cvToHex,
  cvToString,
  FungibleConditionCode,
  hexToCV,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  SignedContractCallOptions,
  sponsorTransaction,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { handleTransaction, network } from "../src/deploy";
import {
  accountsApi,
  boombox_admin,
  contractsApi,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";
import { c32ToB58 } from "c32check";
import { writeFileSync } from "fs";

const { poolAdmin, fpoo1Payout } = keys;
const dryrun = false;
const sendManyFee = 20_000;

const payoutInStx = true;
const boomboxId = 17;
const numberOfBoomboxes = 76;

const totalRewards = 235608239;
const totalRewardsSATS = 341867;

const payoutStacksTip = 95797;
const cycleId = 52;

const boomboxAdminNFTs = (
  payoutInStx
    ? [
        Array.from({ length: 20 }, (_, k) => k + 1),
        Array.from({ length: 20 }, (_, k) => k + 21),
        Array.from({ length: 20 }, (_, k) => k + 41),
        Array.from({ length: numberOfBoomboxes - 61 + 1 }, (_, k) => k + 61),
      ]
    : // use this for details in btc payout
      [Array.from({ length: numberOfBoomboxes }, (_, k) => k + 1)]
).map((nftIds) => {
  return {
    boomboxId,
    nftIds,
    totalRewards,
    totalRewardsSATS: totalRewardsSATS,
  };
});

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce: number,
  sponsorNonce: number,
  memo: string
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          return tupleCV({
            to: standardPrincipalCV(receiver),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from(memo)),
          });
        })
      ),
    ],
    senderKey: fpoo1Payout.private,
    nonce: nonce ? new BN(nonce) : undefined,
    fee: new BN(0),
    network,
    sponsored: true,
    postConditions: [
      makeStandardSTXPostCondition(
        fpoo1Payout.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(sendManyFee),
      sponsorPrivateKey: poolAdmin.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result });
  }
}

async function logAndPayout(
  stackerSet: {
    amount: number;
    stacker: string;
    reward: number;
  }[],
  nonce: number,
  sponsorNonce: number,
  memo: string
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  console.log({
    stackerSetLength: stackerSet.length,
    totalStackedSet,
    totalPayoutSet,
    nonce,
    sponsorNonce,
  });
  await payoutSet(stackerSet, totalPayoutSet, nonce, sponsorNonce, memo);
}

let actualTotalPayout = 0;
const fn = async () => {
  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });

  //
  // edit here for nonce
  //
  let nonce = accountInfo.nonce;
  let sponsorNonce = sponsorAccountInfo.nonce;

  for (let [index, boombox] of boomboxAdminNFTs.entries()) {
    const details: any = await callReadOnlyFunction({
      contractAddress: boombox_admin.address,
      contractName: boombox_admin.name,
      functionName: "get-boombox-by-id",
      functionArgs: [uintCV(boombox.boomboxId)],
      senderAddress: boombox_admin.address,
      network: network,
    });
    const [contractAddress, contractName] = cvToString(
      details.value.data["fq-contract"]
    ).split(".");

    const totalStackedHex: any = await contractsApi.getContractDataMapEntry({
      contractAddress: boombox_admin.address,
      contractName: boombox_admin.name,
      mapName: "total-stacked",
      key: cvToHex(uintCV(boombox.boomboxId)),
      proof: 0,
    });
    // console.log(totalStackedHex);
    const totalStackedCV = hexToCV(totalStackedHex.data) as any;
    const totalStacked = Number(totalStackedCV.value.value);

    if (payoutInStx) {
      // get data for set of nfts
      const totalStackedSetCV: any = await callReadOnlyFunction({
        contractAddress: boombox_admin.address,
        contractName: boombox_admin.name,
        functionName: "get-total-stacked-ustx-at-block",
        functionArgs: [
          uintCV(boombox.boomboxId),
          listCV(boombox.nftIds.map((id) => uintCV(id))),
          uintCV(payoutStacksTip),
        ],
        senderAddress: boombox_admin.address,
        network: network,
      });
      const totalStackedSet = Number(
        totalStackedSetCV.value.data["total"].value
      );

      console.log({
        boomboxId: boombox.boomboxId,
        totalStacked,
        totalRewards: boombox.totalRewards,
        boomboxSetTotalStacked: totalStackedSet,
      });
    }

    const receivers = [];
    for (let nftId of boombox.nftIds) {
      const nftDetails: any = await callReadOnlyFunction({
        contractAddress: boombox_admin.address,
        contractName: boombox_admin.name,
        functionName: "nft-details-at-block",
        functionArgs: [
          uintCV(boombox.boomboxId),
          contractPrincipalCV(contractAddress, contractName),
          uintCV(nftId),
          uintCV(payoutStacksTip),
        ],
        senderAddress: boombox_admin.address,
        network: network,
      });
      try {
        const amount = Number(nftDetails.value.data["stacked-ustx"].value);
        receivers.push({
          stacker: cvToString(nftDetails.value.data["owner"].value),
          amount,
          reward: Math.round((boombox.totalRewards * amount) / totalStacked),
        });
      } catch (e) {
        console.log(nftDetails, boombox.boomboxId, nftId, e);
        throw e;
      }
    }

    // get details for payout in btc
    writeFileSync(
      `cycle-${cycleId}-boombox-admin-${boombox.boomboxId}.csv`,
      receivers
        .map((s) => {
          const amount = Math.floor(
            (s.amount * boombox.totalRewardsSATS) / totalStacked
          );
          if (amount > 3000) {
            return `${c32ToB58(s.stacker)}, ${amount}, ${s.stacker}`;
          } else {
            return `${c32ToB58(s.stacker)}, 3000, ${s.stacker}`;
          }
        })
        .join("\n")
    );

    //
    // do send payout in stx
    //
    actualTotalPayout += receivers.reduce(
      (sum: number, s) => sum + s.reward,
      0
    );
    await logAndPayout(
      receivers,
      nonce + index,
      sponsorNonce + index,
      `Rewards cycle #${cycleId}`
    );
  }
  console.log({ actualTotalPayout });
};

fn();
