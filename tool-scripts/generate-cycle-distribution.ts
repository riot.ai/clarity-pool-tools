const totalRewardsBTC = 1.30602600;
const dustAmountSats = 66_200;
const newDustAmountSats = dustAmountSats - 79_000;
const consolidations = [
  {
    btcTx: "a968c8ce6d2f3d061e5ac4a3f6b5ed8401de879d00a8c11a4e8fca63ba749f73",
    btcAmount: 0.84361280,
    btcFees: 0.00511720,
  },
  {
    btcTx: "f6a40ea4e5b409686a154d78b2ce45953b9a61e978cc0d5ac9db8d630c7734d8",
    btcAmount: 0.45492728,
    btcFees: 0.00249672,
  },
];

const swaps = [
  {
    btcTx: "23d824ffb4f764cefee6fcd1fcc9266a1c8b0aed5fcfa1a4302c690412a175ec",
    stxTx: "ecf8f7b1f45331d4e68fbb759bae8227448d56fc2f9c6e8d502ecd12af3e4886",
    btcFeesSats: 2664,
    btcAmount: 0.84,
    stxAmount: 16056.129939,
  },
  {
    btcTx: "55d59cb5bd46627f9bbaed51d127f029647b967697358bf05f97d02a69d1cb62",
    stxTx: "e848306b94174b5f5bec52470ad0db8922fe393eabbac39e77401f4d7fed294d",
    btcFeesSats: 3468,
    btcAmount: 0.45846,
    stxAmount: 9513.3492,
  }
];
const totalStacked = 46679844666457;
const stxDistributed = 25569478371;
const poolMembers = 1452;
const contracts = 4;
const cycleId = 81;
const reserveBTC = 0.00002427;
const reserveBTCFees = 0;
const reserveBTCTx =
  "";

const difference =
  totalRewardsBTC -
  consolidations.reduce((acc, c) => acc + c.btcAmount + c.btcFees, 0) -
  newDustAmountSats / 1e8;
console.log(Number(difference).toFixed(8));

const difference2 =
  totalRewardsBTC -
  consolidations.reduce((acc, c) => acc + c.btcFees, 0) -
  swaps[0].btcAmount -
  swaps[0].btcFeesSats / 1e8 -
  swaps[1].btcAmount -
  swaps[1].btcFeesSats / 1e8 -
  reserveBTC -
  reserveBTCFees / 1e8 -
  newDustAmountSats / 1e8;
console.log(Number(difference2).toFixed(8));

const consolidationsText =
  consolidations.length === 2
    ? `
Consolidation Part 1

After 1 week, we

consolidated ${consolidations[0].btcAmount} BTC (${consolidations[0].btcFees} BTC fees) to proxy address:
https://mempool.space/tx/${consolidations[0].btcTx}

Consolidation Part 2

After the cycle, we

consolidated ${consolidations[1].btcAmount} BTC (${consolidations[1].btcFees} BTC fees) to proxy address: https://mempool.space/tx/${consolidations[1].btcTx}

`
    : `
Consolidation
    
    After the cycle, we

    consolidated ${consolidations[0].btcAmount} BTC (${consolidations[0].btcFees} BTC fees) to proxy address: https://mempool.space/tx/${consolidations[0].btcTx}
        `;

const swap = (i:number) => {
          return `${swaps[i].btcAmount} BTC to ${Number(swaps[i].stxAmount).toLocaleString(
            "en-US",
            {
              useGrouping: true,
            }
          )} STX via aBTC:
          a) BTC to swap pool: https://mempool.space/tx/${swaps[i].btcTx}
          b) swap pool to STX: https://explorer.hiro.so/txid/0x${
            swaps[i].stxTx
          }?chain=mainnet
`;
        }
const text = `
We have received ${totalRewardsBTC} BTC as rewards.

Dust transactions

There are many dust rewards of 200 sats and 550 sats. We started to ignore these utxos because consolidating them is more expensive then their value. That means that a few sats will rest in the pox reward address. For this cycle, there were ${Number(
  dustAmountSats
).toLocaleString(
  "en-US"
)} sats left in the wallet after reward distribution. This is something to keep in mind when doing accounting.

${consolidationsText}

Total Rewards

After the first consolidation we swapped

${swap(0)}

After the second consolidation we swapped

${swap(1)}

A total of ${Number(stxDistributed).toLocaleString("en-US", {
  useGrouping: true,
})} STX were distributed to ${
  poolMembers - contracts
} pool members and ${contracts} contracts. The distribution was based on ${Number(
  totalStacked / 1000000
).toLocaleString("en-US", {
  useGrouping: true,
})} STX.

Preparing for Nakamoto Release (5% reserve)

This year, the Nakamoto hard fork will bring sBTC. This means that we will replace aBTC with sBTC. The flow will be similar: wrapping to sBTC, then exchanging to STX and distributing.

However, stackers need to do more work eventually, run stacks and a signer node. We are preparing for that and put around 5% of the rewards aside. We started to build up infrastructure and it will allow us to operate Stacks 3.0 signer nodes.

For cycle #${cycleId}, we sent ${reserveBTC} https://mempool.space/tx/${reserveBTCTx} to the reserve. That is around 5% of the consolidated BTC rewards.

Wrapping using aBTC

The rewards were wrapped from BTC to STX via aBTC. This worked quite well and fast. It is transparent and minting aBTC happens automagically. You can read more about aBTC at https://docs.alexgo.io/bitcoin-bridge/abtc-a.k.a-alex-btc
`;

console.log(text);
