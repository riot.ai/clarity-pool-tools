import {
  AccountsApi,
  ReadOnlyFunctionArgsFromJSON,
} from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  callReadOnlyFunction,
  cvToHex,
  cvToString,
  FungibleConditionCode,
  hexToCV,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  sponsorTransaction,
  uintCV,
  deserializeTransaction,
  ContractCallPayload,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import {
  accountsApi,
  boomboxes,
  boomboxes2,
  contractsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";
import * as pg from "pg";
import { assert } from "console";

const BN = require("bn.js");

const { poolAdmin, fpoo1Payout } = keys;

var re = new RegExp(".{1,2}", "g");

(async () => {
  const client = new pg.Client({
    port: 5555,
    user: "postgres",
    password: "postgres",
  });
  await client.connect();

  const satsOnStx = await client
    .query(
      `select * from txs t where 
      contract_call_contract_id = 'SP6P4EJF0VG8V0RB3TQQKJBHDQKEF6NVRD1KZE3C.stacksbridge-satoshibles'
      and contract_call_function_name in ('release', 'release-many', 'lock', 'lock-many')
      and canonical and microblock_canonical 
      order by block_height, tx_index`
    )
    .then((r) =>
      r.rows.map((row) => {
        const payload = deserializeTransaction(row.raw_tx.toString("hex"))
          .payload as ContractCallPayload;
        return payload;
      })
    );

  console.log(
    satsOnStx.map(
      (payload) =>
        (payload.functionName.content.startsWith("release") ? "+" : "-") +
        cvToString(payload.functionArgs[0]).substring(1) + " " + (payload.functionArgs[1] ? cvToString(payload.functionArgs[1]) : "")
    )
  );

  await client.end();
})();
