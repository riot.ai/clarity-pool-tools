import { readFileSync } from "fs";
import { network } from "../src/deploy";
import { downloadRewards } from "../src/pool-tool-utils";

const rewardAddress = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";

const downloadFromStacksNode = async (cycle: number) => {
  const rewards = await downloadRewards(network, rewardAddress);
  for (let i = 0; i < 30; i++) {
    const start = 668150 + i * 2100;
    const end = start + 2100;
    const rewardKeysPerCyle = Object.keys(rewards).filter((k) => {
      return (
        rewards[k].burn_block_height > start &&
        rewards[k].burn_block_height <= end
      );
    });
    const total = rewardKeysPerCyle.reduce((total: number, k: any) => {
      return total + parseInt(rewards[k].reward_amount);
    }, 0);
    console.log(i + 1, total, rewardKeysPerCyle.length, start, end);
    if (rewardKeysPerCyle.length > 0) {
      console.log(
        rewardKeysPerCyle.reduce((min: number, k: any) => {
          return rewards[k].burn_block_height < min
            ? rewards[k].burn_block_height
            : min;
        }, Number.MAX_VALUE),
        rewardKeysPerCyle.reduce((max: number, k: any) => {
          return rewards[k].burn_block_height > max
            ? rewards[k].burn_block_height
            : max;
        }, 0)
      );
    }
  }
};

const logRewards = async (cycle: number) => {
  const from = 666050 + cycle * 2100;
  console.log(from, from + 2000);
  const rewards = JSON.parse(readFileSync(`rewards-${cycle}.json`).toString());
  const txs = rewards.transactions.filter(
    (t: any) => t.blockheight <= from + 2000 && t.category === "receive"
  );
  const slots = new Set(txs.map((t: any) => t.blockheight));
  console.log(slots.size, slots);
  const total = txs.reduce((total: number, t: any) => {
    return t.amount + total;
  }, 0.0);
  console.log(total);
};

downloadFromStacksNode(23).then(async (r) => logRewards(23));
