import { hexToBytes } from "@stacks/common";
import {
  bufferCV,
  bufferCVFromString,
  hexToCV,
  tupleCV,
} from "@stacks/transactions";
import { mainnet, NEXT_CYCLE } from "../src/deploy";
import {
  accountsApi,
  cycleLength,
  firstBurnChainBlock,
  infoApi,
  preparePhaseLength,
  stackDelegatedStxsInBatches,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { fastPool, keys } from "./config";

const { poolAdmin } = keys;

const pools = [{ admin: poolAdmin, lockingPeriod: 1, nonce: 0 }];

const cycleId = NEXT_CYCLE;

async function delegateStack(
  pool: {
    admin: { stacks: string; private: string };
    lockingPeriod: number;
    nonce: number;
  },
  cycleId: number
) {
  // cycle info
  const info = await infoApi.getCoreApiInfo();
  //
  // start block of next cycle
  const startBlock =
    firstBurnChainBlock + cycleId * cycleLength - preparePhaseLength;
  const startBurnHeight = startBlock - 150;
  console.log({
    admin: pool.admin.stacks,
    nonce: pool.nonce,
    startBlock,
    burnBlockHeight: info.burn_block_height,
  });
  const length = 10;
  // Change here start
  const indices = undefined;
  const lockingPeriod = pool.lockingPeriod;
  const admin = pool.admin;
  // change here end

  let accountInfo = await accountsApi.getAccountInfo({
    principal: admin.stacks,
    proof: 0,
  });

  // overwrite nonce
  if (pool.nonce > 0) {
    accountInfo.nonce = pool.nonce;
  }
  console.log(accountInfo.nonce, pool.admin.stacks);
  //const poolAddress = pool.admin.stacks;
  const poolAddress = fastPool.stacks;
  const { countTxsSent } = await stackDelegatedStxsInBatches(
    indices,
    length,
    fastPool.rewardPoxAddrCV,
    poolAddress,
    startBurnHeight,
    lockingPeriod,
    startBlock + cycleLength * lockingPeriod,
    cycleId,
    false,
    admin,
    accountInfo.nonce
  );

  let nextAccountNonce = accountInfo.nonce + countTxsSent;

  for (let i = lockingPeriod - 1; i > 0; i--) {
    const { countTxsSent } = await stackDelegatedStxsInBatches(
      indices,
      length,
      fastPool.rewardPoxAddrCV,
      poolAddress,
      startBurnHeight,
      i,
      startBlock + cycleLength * i + 100,
      cycleId,
      true,
      admin,
      nextAccountNonce
    );
    nextAccountNonce += countTxsSent;
  }
}

(async () => {
  for (let p of pools) {
    delegateStack(p, cycleId).catch((e) => console.log(e));
  }
})();
