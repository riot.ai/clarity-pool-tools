import {
  AnchorMode,
  NonFungibleConditionCode,
  PostConditionMode,
  broadcastTransaction,
  createAssetInfo,
  hexToCV,
  listCV,
  makeContractCall,
  makeStandardNonFungiblePostCondition,
} from "@stacks/transactions";
import { Pool } from "pg";
import { keys } from "./config";

const { poolHelper } = keys;

const query = (
  cycle: number
) => `with mints as (select value, recipient, block_height  from nft_events ne where 
    canonical and microblock_canonical 
    and ne.asset_identifier = 'SM3KNVZS30WM7F89SXKVVFY4SN9RMPZZ9FX929N0V.li-stx-mint-nft::li-stx-mint'
    and asset_event_type_id = 2),
    burns as (select value  from nft_events ne where 
    canonical and microblock_canonical 
    and ne.asset_identifier = 'SM3KNVZS30WM7F89SXKVVFY4SN9RMPZZ9FX929N0V.li-stx-mint-nft::li-stx-mint'
    and asset_event_type_id = 3)
    select encode(mints.value, 'hex') as value, recipient from mints left join burns on mints.value = burns.value
    join blocks b on mints.block_height = b.block_height  
    where b.canonical  
    and burns.value is null
    and (b.burn_block_height - 666050) / 2100 = ${cycle}
    order by mints.value  desc`;

const finalizedMint = async () => {
  const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "postgres",
    password: "postgres",
    port: 5432,
  });

  const result = await pool.query(query(81));
  const idList = result.rows.map((r: any) => {
    console.log(r);
    return hexToCV(r.value);
  });
  const tx = await makeContractCall({
    contractAddress: "SM3KNVZS30WM7F89SXKVVFY4SN9RMPZZ9FX929N0V",
    contractName: "lqstx-mint-endpoint-v1-02",
    functionName: "finalize-mint-many",
    functionArgs: [listCV(idList)],
    senderKey: poolHelper.private,
    anchorMode: AnchorMode.Any,
    fee: 1_000_000,
    postConditionMode: PostConditionMode.Deny,
    postConditions: result.rows.map((r: any) =>
      makeStandardNonFungiblePostCondition(
        r.recipient,
        NonFungibleConditionCode.Sends,
        createAssetInfo(
          "SM3KNVZS30WM7F89SXKVVFY4SN9RMPZZ9FX929N0V",
          "li-stx-mint-nft",
          "li-stx-mint"
        ),
        hexToCV(r.value)
      )
    ),
  });
  const txResult = await broadcastTransaction(tx, "mainnet");
  console.log(txResult);
};

finalizedMint();
