import { writeFileSync } from "fs";
import { network, NEXT_CYCLE } from "../src/deploy";
import {
  downloadFastPoolTxs, downloadPoxTxs, getStackersFromBoomboxAdmin, getStackersFromFastPool, getStackersFromPox, stackersToCycle
} from "../src/pool-tool-utils";
import { fastPool } from "./config";

const cycle = NEXT_CYCLE; // <--- edit here

const poolAdmin = fastPool;
(async () => {
  await downloadPoxTxs(network);
  await downloadFastPoolTxs(network);

  const poxStackers = getStackersFromPox(network, [poolAdmin]);

  const fastPoolStackers = getStackersFromFastPool(cycle, network);

  const stackers = fastPoolStackers
    .concat(poxStackers)

  // only logging
  const stackersInCycle = stackersToCycle(stackers, cycle);
  writeFileSync(`members-${cycle}.json`, JSON.stringify(stackersInCycle));
})();
