import { getPendingPoolMembers } from "./fast-pool-utils";

(async () => {
  const pendingPoolMembers = await getPendingPoolMembers();
  console.log(pendingPoolMembers.length);
  console.log(
    pendingPoolMembers.map((m) => {
      return {
        stacker: m.stacker,
        delegated_to: m.status.details.delegated_to,
        locked: m.s2?.stacked ? m.s2.details.unlock_height : undefined,
        delegated: m.status.details.amount_micro_stx
      };
    })
  );
})();
