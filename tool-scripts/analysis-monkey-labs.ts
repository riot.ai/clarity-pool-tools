import { MapEntryResponse } from "@stacks/blockchain-api-client";
import {
  callReadOnlyFunction,
  ClarityValue,
  cvToHex,
  cvToString,
  hexToCV,
  standardPrincipalCV,
  uintCV,
} from "@stacks/transactions";
import { contractsApi } from "../src/pool-tool-utils";

async function checkCount() {
  const tokenCount: MapEntryResponse =
    await contractsApi.getContractDataMapEntry({
      contractAddress: "SP2KAF9RF86PVX3NEE27DFV1CQX0T4WGR41X3S45C",
      contractName: "bitcoin-monkeys-labs",
      mapName: "token-count",
      key: cvToHex(
        standardPrincipalCV("SP7VK7V27R0H2C7WRR378457WX8VX1Q32RCZRV6H")
      ),
      proof: 0,
    });
  console.log({ tokenCount: cvToString(hexToCV(tokenCount.data)) });

  const owner: ClarityValue = await callReadOnlyFunction({
    contractAddress: "SP2KAF9RF86PVX3NEE27DFV1CQX0T4WGR41X3S45C",
    contractName: "bitcoin-monkeys-labs",
    functionName: "get-owner",
    functionArgs: [uintCV(1107)],
    senderAddress: "SP2KAF9RF86PVX3NEE27DFV1CQX0T4WGR41X3S45C",
  });
  console.log({ owner: cvToString(owner) });
}

checkCount();
