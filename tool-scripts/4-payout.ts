import { ReadOnlyFunctionArgsFromJSON } from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  bufferCV,
  bufferCVFromString,
  createAssetInfo,
  cvToHex,
  cvToString,
  falseCV,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardFungiblePostCondition,
  makeStandardSTXPostCondition,
  noneCV,
  principalCV,
  someCV,
  sponsorTransaction,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import {
  CITY_COIN_REWARDS_CONTRACT,
  SEND_XBTC_MANY_CONTRACT,
  stackerReplaceMap,
  XBTC_CONTRACT,
} from "../src/constants";
import { handleTransaction, network, NEXT_CYCLE } from "../src/deploy";
import {
  accountsApi,
  contractsApi,
  downloadPoolPayoutHints,
  getPayoutHints,
} from "../src/pool-tool-utils";
import { keys } from "./config";
import {
  getStackedAmounts,
  getStackedAmountsFromFile,
} from "./fast-pool-utils";

const BN = require("bn.js");

const { poolAdmin, poolHelper } = keys;
// TODO
const fpoo1Payout = poolAdmin;
const rewardCycle = NEXT_CYCLE - 1;

const testRun = true; // <---------- edit here

const distributeToCityCoins = true;
const distributeToStandardStackers = true;

const sendManyFee = 100_000;
const payoutFeeXBTCContract = 100_000;
const payoutFeeSendManyContract = 100_000;

const rewardsBTC = 1.2399082;
let totalRewardsBTC = rewardsBTC;
let totalRewards1 = 16056129939;
let totalRewards2 = 9513349200;
let totalRewards = totalRewards1 + totalRewards2;
/*let totalRewards = Math.floor(
  (rewardsBTC * 100000000 * 1000000) / exchangeRate
);*/
const exchangeRate = Math.round(
  (rewardsBTC * 100_000_000) / (totalRewards / 1_000_000)
);

/**
 * Payout set of stackers in stx
 * @param stackersSet
 * @param totalPayoutOfSet
 * @param nonce
 * @param sponsorNonce
 * @param memo
 * @param payoutHints
 */
async function payoutSet(
  stackersSet: { stacker: string; reward: bigint }[],
  totalPayoutOfSet: bigint,
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          if (s.stacker in stackerReplaceMap) {
            receiver = (stackerReplaceMap as any)[s.stacker];
            console.log("replaced by mails", s.stacker, receiver);
          }
          if (s.stacker in payoutHints) {
            const stackerHint = (payoutHints as any)[s.stacker];
            if (stackerHint.recipient) {
              receiver = stackerHint.recipient;
              console.log("replaced by hints", s.stacker, receiver);
            }
          }
          return tupleCV({
            to: principalCV(receiver),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from(memo)),
          });
        })
      ),
    ],
    senderKey: fpoo1Payout.private,
    nonce: nonce ? nonce : undefined,
    fee: 0,
    network,
    sponsored: true,
    postConditions: [
      makeStandardSTXPostCondition(
        fpoo1Payout.stacks,
        FungibleConditionCode.Equal,
        totalPayoutOfSet
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!testRun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(sendManyFee),
      sponsorPrivateKey: poolHelper.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result });
  }
}

async function logAndPayout(
  stackerSet: {
    stackedAmount: bigint;
    stacker: string;
    reward: bigint;
  }[],
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: bigint, s) => sum + s.reward,
    0n
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: bigint, s) => sum + s.stackedAmount,
    0n
  );
  console.log({
    stackerSetLength: stackerSet.length,
    totalStackedSet,
    totalPayoutSet,
    nonce,
  });
  await payoutSet(
    stackerSet,
    totalPayoutSet,
    nonce,
    sponsorNonce,
    memo,
    payoutHints
  );
}

/**
 * Payout stackers in xBTC
 * @param details
 * @param nonce
 * @param sponsorNonce
 * @param memo
 */
async function payoutInXbtc(
  details: { amountXBTC: bigint; recipient: string }[],
  nonce: number,
  sponsorNonce: number,
  memo: string
) {
  let total = details.reduce((sum, d) => sum + d.amountXBTC, 0n);
  let options;
  const useXBTCContract = details.length === 1;
  if (useXBTCContract) {
    const { amountXBTC, recipient } = details[0];
    options = {
      contractAddress: XBTC_CONTRACT.address,
      contractName: XBTC_CONTRACT.name,
      functionName: "transfer",
      functionArgs: [
        uintCV(amountXBTC),
        standardPrincipalCV(fpoo1Payout.stacks),
        standardPrincipalCV(recipient),
        someCV(bufferCVFromString(memo)),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          fpoo1Payout.stacks,
          FungibleConditionCode.Equal,
          new BN(amountXBTC),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  } else {
    options = {
      contractAddress: SEND_XBTC_MANY_CONTRACT.address,
      contractName: SEND_XBTC_MANY_CONTRACT.name,
      functionName: "send-xbtc-many",
      functionArgs: [
        listCV(
          details.map((d) => {
            return tupleCV({
              to: standardPrincipalCV(d.recipient),
              "xbtc-in-sats": uintCV(d.amountXBTC),
              memo: bufferCVFromString(memo),
              "swap-to-ustx": falseCV(),
              "min-dy": noneCV(),
            });
          })
        ),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          fpoo1Payout.stacks,
          FungibleConditionCode.Equal,
          new BN(total),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  }
  const tx = await makeContractCall({
    ...options,
    senderKey: fpoo1Payout.private,
    sponsored: true,
    fee: new BN(0),
    nonce: nonce ? new BN(nonce) : undefined,
    network,

    anchorMode: AnchorMode.Any,
  });

  if (!testRun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(
        useXBTCContract ? payoutFeeXBTCContract : payoutFeeSendManyContract
      ),
      sponsorPrivateKey: poolHelper.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result, sponsorNonce, nonce });
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: fpoo1Payout.stacks,
        arguments: options.functionArgs.map((a) => cvToHex(a)),
      }),
    });
    console.log(
      { result },
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

/**
 * Payout stackers via a contract function
 */
const CITY_COIN_FUNCTIONS: { [key: string]: string } = {
  "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-mia-mining-v2":
    "send-stacking-reward-mia",
  "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-nyc-mining-v2":
    "send-stacking-reward-nyc",
};

async function payoutCityCoins(
  stackers: { stacker: string; reward: bigint }[],
  nonce: number,
  sponsorNonce: number
) {
  let count = 0;
  let skippedRewards = 0n;
  for (let stacker of stackers) {
    const functionName = CITY_COIN_FUNCTIONS[stacker.stacker];
    if (!functionName) {
      if (
        stacker.stacker.indexOf(
          "SM26NBC8SFHNW4P1Y4DFH27974P56WN86C92HPEHH.fastpool-member"
        ) === 0
      ) {
        console.log("LISA: ", stacker.stacker, stacker.reward);
      } else {
        console.log("skipped ", stacker.stacker, stacker);
        skippedRewards += stacker.reward;
      }
      continue;
    }
    let options;
    options = {
      contractAddress: CITY_COIN_REWARDS_CONTRACT.address,
      contractName: CITY_COIN_REWARDS_CONTRACT.name,
      functionName,
      functionArgs: [uintCV(rewardCycle), uintCV(stacker.reward)],
      postConditions: [
        makeStandardSTXPostCondition(
          fpoo1Payout.stacks,
          FungibleConditionCode.Equal,
          stacker.reward
        ),
      ],
    };

    const tx = await makeContractCall({
      ...options,
      senderKey: fpoo1Payout.private,
      sponsored: true,
      fee: new BN(0),
      nonce: nonce + count,
      network,

      anchorMode: AnchorMode.Any,
    });

    if (!testRun) {
      const sponsoredTx = await sponsorTransaction({
        transaction: tx,
        fee: new BN(payoutFeeXBTCContract),
        sponsorPrivateKey: poolHelper.private,
        sponsorNonce: sponsorNonce + count,
      });
      const result = await handleTransaction(sponsoredTx);
      console.log({
        result,
        sponsorNonce: sponsorNonce + count,
        nonce: nonce + count,
      });
    } else {
      const result = await contractsApi.callReadOnlyFunction({
        contractAddress: options.contractAddress,
        contractName: options.contractName,
        functionName: options.functionName,
        readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
          sender: fpoo1Payout.stacks,
          arguments: options.functionArgs.map((a) => cvToHex(a)),
        }),
      });
      console.log(
        { result },
        { options },
        (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
      );
    }
    count += 1;
  }
  if (skippedRewards > 0n) {
    console.log("===============");
    console.log("skipped rewards", skippedRewards);
  }
  return count;
}

async function payoutLisa(
  stackers: { stacker: string; stackedAmount: bigint; reward: bigint }[],
  nonce: number,
  sponsorNonce: number,
  memo: string
) {
  const lisaMembers = stackers.filter(
    (s) =>
      s.stacker.indexOf(
        "SM26NBC8SFHNW4P1Y4DFH27974P56WN86C92HPEHH.fastpool-member"
      ) === 0
  );
  return logAndPayout(lisaMembers, nonce, sponsorNonce, memo, {});
}

/**
 * MAIN FUNCTION
 */
const payout = async () => {
  const memo = `reward cycle #${rewardCycle}`;
  console.log("******");
  console.log(memo);
  const stackedAmounts = await getStackedAmountsFromFile(rewardCycle);
  const allStackers = Object.keys(stackedAmounts);

  console.log(allStackers[0], stackedAmounts[allStackers[0]]);
  let totalStacked = 0n;
  for (let stacker of allStackers) {
    totalStacked = totalStacked + stackedAmounts[stacker].stackedAmount;
  }

  const isStandardPrincipal = (s: string) => s.indexOf(".") < 0;
  const isContractPrincipal = (s: string) => s.indexOf(".") >= 0;

  const stackersContracts = allStackers.filter(isContractPrincipal);
  const stackersStandard = allStackers.filter(isStandardPrincipal);

  //
  // get nonce
  //
  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolHelper.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  let sponsorNonce = sponsorAccountInfo.nonce;
  // ({ nonce, sponsorNonce } = { nonce: 149, sponsorNonce: 5821 });

  console.log({ nonce, sponsorNonce });

  // distribute to city coins
  const { count, stats: statsContracts } = distributeToCityCoins
    ? await payoutStackers(
        true,
        stackersContracts,
        stackedAmounts,
        totalStacked,
        allStackers,
        memo,
        nonce,
        sponsorNonce
      )
    : { count: 0, stats: { distributedRewards: 0n } };

  // distribute to standard stackers

  const { stats: statsStandard } = distributeToStandardStackers
    ? await payoutStackers(
        false,
        stackersStandard,
        stackedAmounts,
        totalStacked,
        allStackers,
        memo,
        nonce + count,
        sponsorNonce + count
      )
    : { stats: { distributedRewards: 0n } };

  console.log(statsContracts);
  console.log(statsStandard);
  console.log({
    totalDistributedRewards:
      statsContracts.distributedRewards + statsStandard.distributedRewards,
    poolAddress: fpoo1Payout.stacks,
    poolHelperAddress: poolHelper.stacks,
  });
};

const payoutStackers = async (
  isPayoutForContracts: boolean,
  stackers: string[],
  stackedAmounts: {
    [key: string]: {
      cycleId: number;
      stackedAmount: bigint;
      unlockHeight: number;
      timestamp: string;
      txid: string;
    };
  },
  totalStacked: bigint,
  allStackers: string[],
  memo: string,
  nonce: number,
  sponsorNonce: number
) => {
  const stackersWithRewards: {
    stacker: string;
    stackedAmount: bigint;
    reward: bigint;
  }[] = [];
  for (let stacker of stackers) {
    const stackedAmount = stackedAmounts[stacker].stackedAmount;
    const reward = (BigInt(totalRewards) * stackedAmount) / totalStacked;
    if (isPayoutForContracts) {
      console.log(reward, stackedAmount, totalStacked);
    }
    stackersWithRewards.push({
      stacker,
      stackedAmount,
      reward,
    });
  }

  const stats = {
    isPayoutForContracts,
    totalStacked,
    numberOfStackers: allStackers.length,
    exchangeRate,
    precentage: totalRewards / Number(totalStacked),
    numberOfPaidStackers: stackers.length,
    distributedRewards: stackersWithRewards.reduce(
      (sum, s) => sum + s.reward,
      0n
    ),
  };

  await downloadPoolPayoutHints(network);
  const payoutHints = getPayoutHints();

  const xbtcReceiverStackers: {
    stacker: string;
    stackedAmount: bigint;
    reward: bigint;
  }[] = [];
  console.log({ isPayoutForContracts });

  //
  // payout in stx
  //
  let count;
  if (isPayoutForContracts) {
    count = await payoutCityCoins(stackersWithRewards, nonce, sponsorNonce);

    // payout to lisa in 1 tx
    await payoutLisa(
      stackersWithRewards,
      nonce + count,
      sponsorNonce + count,
      memo
    );
    count +=1;
  } else {
    // number of tx = mod 200 + 1 tx for the remainder
    let numberOfSendManyTxs = Math.floor(stackersWithRewards.length / 200) + 1;
    for (let i = 0; i < numberOfSendManyTxs; i++) {
      const set = stackersWithRewards.slice(i * 200, (i + 1) * 200);
      await logAndPayout(set, nonce + i, sponsorNonce + i, memo, payoutHints);
    }

    //
    // payout in xbtc
    //
    let numberOfSendManyxBTCTxs =
      Math.floor(xbtcReceiverStackers.length / 20) + 1;
    numberOfSendManyxBTCTxs = 0;
    for (let i = 0; i < numberOfSendManyxBTCTxs; i++) {
      const set = xbtcReceiverStackers.slice(i * 20, (i + 1) * 20).map((s) => {
        return {
          amountXBTC:
            (s.stackedAmount * BigInt(totalRewardsBTC) * 100_000_000n) /
            totalStacked,
          recipient: s.stacker,
        };
      });
      await payoutInXbtc(
        set,
        nonce + numberOfSendManyTxs + i,
        sponsorNonce + numberOfSendManyTxs + i,
        memo
      );
    }
    count = numberOfSendManyTxs + numberOfSendManyxBTCTxs;
  }
  return { count, stats };
};

payout().catch((e) => console.log(e));
