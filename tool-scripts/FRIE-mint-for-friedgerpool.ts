import { mint } from "../src/frie-tokens";
import { keys } from "./config";

const { poolAdmin } = keys;

(async () => {
  await mint(1379_000_000, poolAdmin);
})();
