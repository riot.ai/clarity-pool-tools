import { StacksMainnet } from "@stacks/network";
import {
  AnchorMode,
  PostConditionMode,
  broadcastTransaction,
  listCV,
  makeContractCall,
  principalCV,
} from "@stacks/transactions";
import { network } from "../src/deploy";
import { accountsApi } from "../src/pool-tool-utils";
import { fastPool, keys } from "./config";
import { getPendingPoolMembers } from "./fast-pool-utils";
import { bytesToHex } from "@stacks/common";
import { readFileSync } from "fs";
(async () => {
  const mainnet = new StacksMainnet();

  const { poolHelper } = keys;
  const [contractAddress, contractName] = fastPool.stacks.split(".");

  // define pending members and nonce
  //

  const pendingContracts = false;

  let pendingPoolMembers: { stacker: string }[] = pendingContracts
    ? [
        {
          stacker:
            "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-mia-mining-v2",
        },
        {
          stacker:
            "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-nyc-mining-v2",
        },
      ]
    : await getPendingPoolMembers();

  /*
  pendingPoolMembers = readFileSync("pending-members-76-3.csv")
    .toString()
    .split("\n")
    .map((l:string) => {
      return {
        stacker: l.trim(),
      };
    });
*/

  console.log(pendingPoolMembers.length);
  if (pendingPoolMembers.length === 0) {
    return;
  }

  //
  // get nonce
  //
  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolHelper.stacks,
    proof: 0,
  });

  let nonce = accountInfo.nonce;
  //nonce = 6063;
  console.log({ nonce }, poolHelper.stacks);

  // extend members in batches of 30 (max)
  for (let i = 0; i <= pendingPoolMembers.length / 30; i++) {
    let batch = pendingPoolMembers.slice(i * 30, (i + 1) * 30);
    console.log(batch.map((m) => m.stacker));
    const tx = await makeContractCall({
      contractAddress,
      contractName,
      functionName: "delegate-stack-stx-many",
      functionArgs: [listCV(batch.map((m) => principalCV(m.stacker)))],
      network: mainnet,
      anchorMode: AnchorMode.Any,
      postConditionMode: PostConditionMode.Allow,
      fee: 2_500_000, // <-- fees
      nonce: nonce + i,
      senderKey: poolHelper.private,
    });

    //console.log(bytesToHex(tx.serialize()));
    //console.log("\n");
    //console.log(tx.txid());
    const result = await broadcastTransaction(tx, network);
    if (result.reason as any === "TooMuchChaining") {
      console.log("TooMuchChaining");
      return;
    }
    console.log(result, nonce + i);
    const resultMain = await broadcastTransaction(tx, mainnet);
    console.log(resultMain);
  }
})();
