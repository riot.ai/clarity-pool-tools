import { network } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  logPoolToolContractCSV,
  logStackersCSV,
} from "../src/pool-tool-utils";

(async () => {
  await downloadPoolToolTxs(network);
  const stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    undefined,
    "SPSTX06BNGJ2CP1F6WA8V49B6MYD784N6YZMK95G"
  );
  console.log(stackers.length);
  logStackersCSV(stackers)
})();
