import {
  AccountsApi,
  ReadOnlyFunctionArgsFromJSON,
} from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  callReadOnlyFunction,
  cvToHex,
  cvToString,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  sponsorTransaction,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import {
  accountsApi,
  boomboxes,
  boomboxes2,
  contractsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";
import * as pg from "pg";
import { assert } from "console";

const BN = require("bn.js");

const { poolAdmin, fpoo1Payout } = keys;

var re = new RegExp(".{1,2}", "g");

(async () => {
  const client = new pg.Client({
    port: 5555,
    user: "postgres",
    password: "postgres",
  });
  await client.connect();

  const otherTransfers = await client
    .query(
      `select token_transfer_recipient_address, token_transfer_amount, encode(token_transfer_memo, 'hex') as token_transfer_memo, (select distinct on (address) name from names 
      where address  = token_transfer_recipient_address
      order by address, id desc
      ) from txs where tx_id in (select tx_id  from stx_events se 
        where se.asset_event_type_id = 1
        and sender in ('${poolAdmin.stacks}', '${fpoo1Payout.stacks}')
        )
        and canonical = true and microblock_canonical = true
        and status > 0
        and token_transfer_amount > 0
        and token_transfer_recipient_address not in ('SP1JSH2FPE8BWNTP228YZ1AZZ0HE0064PS6RXRAY4','SP700C57YJFD5RGHK0GN46478WBAM2KG3A4MN2QJ', 'SP287WSB9DMKVWND9JQKZ3H9TY9MSDV9QEQ8SJ3TP', 'SP497E7RX3233ATBS2AB9G4WTHB63X5PBSP5VGAQ', 'SP3AQDW78BWS2AZMBZWD7XWGH5HQS3HHVS2MC9NBD' )
      `
    )
    .then((r) =>
      r.rows.map((row) => {
        return {
          recipient: row.token_transfer_recipient_address,
          amount: parseInt(row.token_transfer_amount),
          memo: String.fromCharCode(...row.token_transfer_memo.match(re).map((s: string) => parseInt(s, 16)).filter((c: number) => c > 0)),
          name: row.name
        };
      })
    );

  console.log(otherTransfers.map(t => `${t.recipient}, ${t.amount / 1_000_000}, ${t.memo}, ${t.name || ''}`));


  const rewardTxs = await client
  .query(
    `select token_transfer_recipient_address, token_transfer_amount, encode(token_transfer_memo, 'hex') as token_transfer_memo, (select distinct on (address) name from names 
    where address  = token_transfer_recipient_address
    order by address, id desc
    ) from txs where tx_id in (select tx_id  from stx_events se 
      where se.asset_event_type_id = 1
      and sender in ('${poolAdmin.stacks}', '${fpoo1Payout.stacks}')
      )
      and canonical = true and microblock_canonical = true
      and status > 0
      and token_transfer_amount > 0
      and token_transfer_recipient_address not in ('SP1JSH2FPE8BWNTP228YZ1AZZ0HE0064PS6RXRAY4','SP700C57YJFD5RGHK0GN46478WBAM2KG3A4MN2QJ', 'SP287WSB9DMKVWND9JQKZ3H9TY9MSDV9QEQ8SJ3TP', 'SP497E7RX3233ATBS2AB9G4WTHB63X5PBSP5VGAQ', 'SP3AQDW78BWS2AZMBZWD7XWGH5HQS3HHVS2MC9NBD' )
    `
  )
  .then((r) =>
    r.rows.map((row) => {
      return {
        recipient: row.token_transfer_recipient_address,
        amount: parseInt(row.token_transfer_amount),
        memo: String.fromCharCode(...row.token_transfer_memo.match(re).map((s: string) => parseInt(s, 16)).filter((c: number) => c > 0)),
        name: row.name
      };
    })
  );

  await client.end();
})();
