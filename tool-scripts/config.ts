import { bytesToHex, hexToBytes } from "@stacks/common";
import { bufferCV, tupleCV } from "@stacks/transactions";
import { readFileSync } from "fs";
import { mainnet } from "../src/deploy";
import { poxCVToBtcAddress } from "../src/utils-pox-addr";
import { decodeBtcAddress, poxAddressToBtcAddress } from "@stacks/stacking";

if (mainnet) {
  //throw new Error("Config only for testnet");
}

export const fastPool = {
  stacks: "SP21YTSM60CAY6D011EZVEVNKXVW8FVZE198XEFFP.pox-fast-pool-v2",
  rewardPoxAddrCV: tupleCV({
    hashbytes: bufferCV(hexToBytes("83ed66860315e334010bbfb76eb3eef887efee0a")),
    version: bufferCV(hexToBytes("04")),
  }),
};

console.log(poxCVToBtcAddress(fastPool.rewardPoxAddrCV));

console.log(
  bytesToHex(
    decodeBtcAddress("bc1qs0kkdpsrzh3ngqgth7mkavlwlzr7lms2zv3wxe").data
  ),
  decodeBtcAddress("bc1qs0kkdpsrzh3ngqgth7mkavlwlzr7lms2zv3wxe").version
);


const poolAdmin = JSON.parse(
  readFileSync(
    "keys-pool-admin.json"
  ).toString()
);

const poolHelper = JSON.parse(
  readFileSync(
    "keys-helper.json"
  ).toString()
);

export const keys = {
  poolAdmin,
  poolHelper,
};
