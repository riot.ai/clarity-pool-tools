import { network } from "../src/deploy";
import { downloadAccountTxs } from "../src/pool-tool-utils";
import { fastPool, keys } from "./config";
import { logTxsCSV } from "./fast-pool-utils";

const { poolHelper } = keys;

(async () => {
  await downloadAccountTxs(poolHelper.stacks, network);
  await downloadAccountTxs(fastPool.stacks, network);
  logTxsCSV(network);
})();
