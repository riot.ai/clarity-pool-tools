import { downloadMiamicoinTxs, logCitycoinCSV } from "../src/citycoins";

(async () => {
    await downloadMiamicoinTxs();
    await logCitycoinCSV();
  })();