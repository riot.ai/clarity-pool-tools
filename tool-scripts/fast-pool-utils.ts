import { StacksNetwork } from "@stacks/network";
import {
  DelegationInfo,
  StackerInfo,
  StackingClient,
  decodeBtcAddress,
  extractPoxAddressFromClarityValue,
  poxAddressToBtcAddress,
  poxAddressToTuple,
} from "@stacks/stacking";
import { ListCV, hexToCV } from "@stacks/transactions";
import { readFileSync, writeFileSync } from "fs";
import * as pg from "pg";
import { NEXT_CYCLE, local, network } from "../src/deploy";
import { fastPool } from "./config";
import axios from "axios";
import { poxCVToBtcAddress } from "../src/utils-pox-addr";

async function createPgClient() {
  const client = new pg.Client({
    port: 5432,
    user: "postgres",
    password: "postgres",
  });
  await client.connect();
  return client;
}
export async function getPendingPoolMembers() {
  const minimumUnlockBurnHeightForCycle = 666050 + 2100 * NEXT_CYCLE + 100;
  console.log({ minimumUnlockBurnHeightForCycle });
  const query = `with vars as (select ${NEXT_CYCLE} as cycle, ${NEXT_CYCLE} * 2100 + 666050 as cycle_start),
  --- revoked stackers tx
  revoked_stackers as (select distinct on (sender_address) block_height, sender_address from txs t
  where canonical  and microblock_canonical and 
  t.contract_call_contract_id  = 'SP000000000000000000002Q6VF78.pox-3' and t.contract_call_function_name = 'revoke-delegate-stx' 
  order by sender_address, block_height desc),
  --- fast pool members
  fp_members as (
  select pe.block_height, pe.microblock_sequence, pe.tx_index, pe.event_index, 
  locked, burnchain_unlock_height::integer,
  (burn_block_height - 666050)/ 2100 as cycle_id,
  to_char(to_timestamp(burn_block_time::bigint), 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') as timestamp,
  encode(tx_id, 'hex') as tx_id,
  stacker from pox3_events pe join blocks b on (pe.block_height = b.block_height), vars
  --
  where pe.canonical and microblock_canonical 
  and pox_addr  = 'bc1qs0kkdpsrzh3ngqgth7mkavlwlzr7lms2zv3wxe'
  and name != 'delegate-stx'
  and name not like 'stack-aggregation%'
  and b.burn_block_height < vars.cycle_start
  and b.burn_block_height > vars.cycle_start - 2100
  order by b.block_height desc, microblock_sequence desc, tx_index desc, event_index desc),
  -- latest locking
  fp_member_details as (
  select distinct on (stacker) *
  from fp_members
  order by stacker, block_height desc, microblock_sequence desc, tx_index desc, event_index desc
  ),
  --- pox-3 events for joining any pool
  event_rows as (select pe.block_height, pe.microblock_sequence, pe.tx_index, pe.event_index, 
  locked, burnchain_unlock_height::integer,
  (burn_block_height - 666050)/ 2100 as cycle_id,
  to_char(to_timestamp(burn_block_time::bigint), 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') as timestamp,
  encode(tx_id, 'hex') as tx_id,
  delegate_to,
  unlock_burn_height,
  stacker from pox3_events pe join blocks b on (pe.block_height = b.block_height), vars
  ------- 
  where pe.canonical and microblock_canonical  
  and name = 'delegate-stx'
  order by b.block_height desc, microblock_sequence desc, tx_index desc, event_index desc),
  -- pox events for joining the latest pool
  stacker_details as (select distinct on (stacker) *
  from event_rows
  order by stacker, block_height desc, microblock_sequence desc, tx_index desc, event_index desc)
  --
  --
  -- delegated stackers tp fast pool who did not revoke or revoked before the delegation
  -- and who are not yet locked.
  select d.stacker
  --, *, count(*) over(), sum(d.locked) over() 
  from stacker_details d 
  left join revoked_stackers r on (d.stacker = r.sender_address) 
  left join fp_member_details p on (d.stacker = p.stacker), vars
  where 
  d.delegate_to = 'SP21YTSM60CAY6D011EZVEVNKXVW8FVZE198XEFFP.pox-fast-pool-v2'
  and (d.unlock_burn_height is null or  d.unlock_burn_height >= vars.cycle_start + 2100)
  and (r.block_height is null or d.block_height > r.block_height)
  and p.stacker is null
  order by d.locked desc`;

  const client = await createPgClient();
  const pendingPoolMemberQueryResult = await client.query(query);
  let pendingPoolMembers = await Promise.all(
    pendingPoolMemberQueryResult.rows.map((r) => {
      const client = new StackingClient(r.stacker, network);
      return client.getAccountBalance().then((balance) => {
        return client.getDelegationStatus().then((s) => {
          return client.getStatus().then((s2) => {
            return { status: s, s2, stacker: r.stacker, balance };
          });
        });
      });
    })
  );

  pendingPoolMembers = pendingPoolMembers.filter(
    (m) =>
      m.status.delegated &&
      m.status.details.delegated_to ===
        "SP21YTSM60CAY6D011EZVEVNKXVW8FVZE198XEFFP.pox-fast-pool-v2" &&
      (!m.s2.stacked ||
        m.s2.details.unlock_height < minimumUnlockBurnHeightForCycle) &&
        m.balance > 0n
  );

  await client.end();
  return pendingPoolMembers as {
    status: DelegationInfo & { delegated: true };
    s2: StackerInfo;
    stacker: string;
  }[];
}

export function eventsForCycleQuery(
  cycleId: number,
  btcPoxAddress: string,
  offset: number
) {
  const query = `
  with vars as (select ${cycleId} as cycle, ${cycleId} * 2100 + 666050 as cycle_start),
  event_rows as (select pe.block_height, pe.microblock_sequence, pe.tx_index, pe.event_index, 
  locked, burnchain_unlock_height::integer,
  (burn_block_height - 666050)/ 2100 as cycle_id,
  to_char(to_timestamp(burn_block_time::bigint), 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') as timestamp,
  encode(tx_id, 'hex') as tx_id,
  stacker from pox3_events pe join blocks b on (pe.block_height = b.block_height), vars
  --
  where pox_addr  = 'bc1qs0kkdpsrzh3ngqgth7mkavlwlzr7lms2zv3wxe'
  and name != 'delegate-stx'
  and name not like 'stack-aggregation%'
  and pe.canonical and microblock_canonical 
  and b.burn_block_height < vars.cycle_start
  and b.burn_block_height > vars.cycle_start - 2100
  order by b.block_height desc, microblock_sequence desc, tx_index desc, event_index desc),
  --
  stacker_details as (select distinct on (stacker) *
  from event_rows
  order by stacker, block_height desc, microblock_sequence desc, tx_index desc, event_index desc)
  --
  select *, count(*) over(), sum(locked) over() from stacker_details
  --limit 1000
  --offset ${offset}
`;
  console.log(query);
  return query;
}

export async function getStackedAmounts(cycleId: number) {
  if (true) {
    return getStackedAmountsFromPgDatabase(cycleId);
  } else {
    return getStackedAmountsFromStacksOnChain(cycleId);
  }
}

export async function getStackedAmountsFromFile(cycleId: number) {
  const membersJson = JSON.parse(
    readFileSync(`members-${cycleId}.json`).toString()
  );
  const members = membersJson.members;
  const stackers: {
    [key: string]: PoolMember;
  } = {};
  members.forEach((m: any) => {
    stackers[m.stacker] = {
      cycleId: m.rewardCycle,
      stackedAmount: BigInt(m.amount),
      unlockHeight: m.unlock,
      timestamp: m.timestamp,
      txid: m.txid,
    };
  });
  return stackers;
}

async function getStackedAmountsFromPgDatabase(cycleId: number) {
  const client = await createPgClient();
  const response = await client.query(
    eventsForCycleQuery(cycleId, poxCVToBtcAddress(fastPool.rewardPoxAddrCV), 0)
  );
  const response2 = await client.query(
    eventsForCycleQuery(
      cycleId,
      poxCVToBtcAddress(fastPool.rewardPoxAddrCV),
      1000
    )
  );
  const stackers: {
    [key: string]: PoolMember;
  } = {};
  for (let r of response.rows) {
    const stacker = r.stacker;
    const existingDetails = stackers[stacker];
    stackers[stacker] = {
      cycleId: r.cycle_id + 1,
      stackedAmount: BigInt(r.locked),
      txid: `0x${r.tx_id}`,
      timestamp: r.timestamp,
      unlockHeight: r.burnchain_unlock_height,
    };
  }
  await client.end();
  return stackers;
}

export interface PoolMember {
  cycleId: number;
  stackedAmount: bigint;
  unlockHeight: number;
  timestamp: string;
  txid: string;
}
const BASE_URL = "https://api.stacksdata.info/v1/sql";
//const BASE_URL = "https://api.statsonchain.com/api/v1/run";

async function getStackedAmountsFromStacksOnChain(cycleId: number) {
  const result = await axios.post(
    BASE_URL,
    eventsForCycleQuery(cycleId, poxCVToBtcAddress(fastPool.rewardPoxAddrCV), 0)
  );
  const result2 = await axios.post(
    BASE_URL,
    eventsForCycleQuery(
      cycleId,
      poxCVToBtcAddress(fastPool.rewardPoxAddrCV),
      1000
    )
  );
  /*
  const result = await axios.post(
    BASE_URL,
    {
      query: eventsForCycleQuery(
        cycleId,
        poxCVToBtcAddress(fastPool.rewardPoxAddrCV)
      ),
    },
    {
      headers: {
        Authorization:
          "eyJzaWduYXR1cmUiOiJhMzk5MjNjMWQ0Nzk4MjE2ZGU5ZmE1ZjVhZjkyNmVlNWVhZmMyOWEwOGQ0N2MyMmI2OGFiZmZhZmYzOTFlMDRkMTZmMWJmZmMwZDdlZDc4Y2Y1YzcyNWVjM2JiMWI2NWRkYzVjNzE3MmY1MTcxZmNiY2YwYzExODIwYThmYzBkNjAwIiwicHVibGljS2V5IjoiMDM2OWVlYmZlOWFhNjhiMzYxOGMxYTNmZTZiYzUxYTA0OTljZWE0YjhiYTQxYmI1ZDIwMzQ1ZDU0YTQ2OTg1YzYyIn0=",
      },
    }
  );
  */
  const stackers: {
    [key: string]: PoolMember;
  } = {};
  const mergeDetails = (
    columns: any,
    index: number,
    details?: { cycleId: number; stackedAmount: bigint }
  ) => {
    switch (columns.name[index]) {
      case "delegate-stack-stx":
        return {
          cycleId: columns.cycle_id[index] + 1,
          stackedAmount: BigInt(columns.lock_amount[index]),
        };
      case "delegate-stack-extend":
        return {
          cycleId:
            (details?.cycleId
              ? details?.cycleId
              : columns.cycle_id[index] + 1) + 1,
          stackedAmount: details?.stackedAmount
            ? details.stackedAmount
            : BigInt(columns.locked[index]),
        };
      case "delegate-stack-increase":
        return {
          cycleId: details?.cycleId
            ? details.cycleId
            : columns.cycle_id[index] + 1,
          stackedAmount: BigInt(columns.locked[index]),
        };
      default:
        throw new Error(`Unknown method ${columns.name[index]}`);
    }
  };
  const addStackerResult = (result: any) => {
    for (let index = 0; index < result.data.columns.stacker.length; index++) {
      const stacker = result.data.columns.stacker[index];
      const existingDetails = stackers[stacker];
      const updatedDetails = mergeDetails(
        result.data.columns,
        index,
        existingDetails
      );
      stackers[stacker] = {
        ...updatedDetails,
        txid: `0x${result.data.columns.tx_id[index]}`,
        timestamp: result.data.columns.timestamp[index],
        unlockHeight: parseInt(
          result.data.columns.burnchain_unlock_height[index]
        ),
      };
    }
  };
  addStackerResult(result);
  addStackerResult(result2);
  return stackers;
}

export function logTxsCSV(network: StacksNetwork) {
  const lines = [];
  const txsFilePath = `acc-txs-${network.chainId}-${fastPool.stacks}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(txs.length);
  lines.push(
    Array.of(
      "time",
      "block",
      "fee-rate",
      "contract",
      "function",
      "sender",
      "txid"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "delegate-stack-stx":
        details = [tx.tx_result.repr].concat(
          tx.contract_call.function_args[0].repr.substring(1) ===
            tx.sender_address,
          tx.contract_call.function_args[0].repr
        );
        break;
      case "delegate-stack-stx-many":
        const l = (hexToCV(tx.contract_call.function_args[0].hex) as ListCV)
          .list.length;
        details = [tx.tx_result.repr, l].concat(
          tx.contract_call.function_args
            ? tx.contract_call.function_args.map((a: any) => a.repr)
            : []
        );
        break;
      default:
        details = [tx.tx_result.repr].concat(
          tx.contract_call.function_args
            ? tx.contract_call.function_args.map((a: any) => a.repr)
            : []
        );
    }
    lines.push(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.fee_rate,
        tx.contract_call.contract_id,
        tx.contract_call.function_name,
        tx.sender_address,
        tx.tx_id,
        ...details
      ).join(",")
    );
  }
  writeFileSync(txsFilePath + ".csv", lines.join("\n"));
}
