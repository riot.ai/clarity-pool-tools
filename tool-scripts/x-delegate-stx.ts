import { StackingClient } from "@stacks/stacking";
import { network } from "../src/deploy";
import { keys } from "./config";

const { poolAdmin } = keys;

const delegate = async (
  stacker: string,
  amountMicroStx: number,
  privateKey: string
) => {
  const client = new StackingClient(stacker, network);
  const result = await client.delegateStx({
    delegateTo: poolAdmin.stacks,
    amountMicroStx,
    privateKey,
  });
  console.log(result);
};

delegate(
  "ST1BY17KAGWQYYZ3TST4CANW0AJFN80YY931DJTK2",
  106_000_000,
  "b6c165948eb7b94bc3785b8c8638aa7788d59b1ea1d856fa04a9bd3a25ca179f01"
);
