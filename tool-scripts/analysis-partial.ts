import {
  callReadOnlyFunction,
  ClarityType,
  cvToString,
  OptionalCV,
  SomeCV,
  TupleCV,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { network, NEXT_CYCLE } from "../src/deploy";
import { getPartialStacked } from "../src/pool-tool-partials";
import { poxContractAddress } from "../src/pool-tool-utils";
import { fastPool } from "./config";

function addPartial(partial: any, total: number) {
  if (partial) {
    if (partial.amount === "none") {
      console.log(partial.cycle, partial.poolAdmin, partial.btcAddress, "none");
    } else {
      const amount = parseInt(partial.amount);
      total += amount;
      console.log(
        partial.cycle,
        partial.poolAdmin,
        partial.btcAddress,
        (amount / 1000000).toLocaleString(undefined, {
          style: "currency",
          currency: "STX",
        })
      );
    }
  }
  return total;
}

(async () => {
  const cycleId = NEXT_CYCLE;
  let total = 0;

  // fast pool
  const partial = await getPartialStacked(
    fastPool.stacks,
    cycleId,
    fastPool.rewardPoxAddrCV
  );
  total = addPartial(partial, total);

  console.log("===================================");
  console.log(
    "total:",
    (total / 1000000).toLocaleString(undefined, {
      style: "currency",
      currency: "STX",
    })
  );

  const [contractAddress, contractName] = fastPool.stacks.split(".");
  let lastAggregationCommit = (await callReadOnlyFunction({
    contractAddress,
    contractName,
    functionName: "get-last-aggregation",
    functionArgs: [uintCV(cycleId)],
    network,
    senderAddress: contractAddress,
  })) as OptionalCV<UIntCV>;

  console.log("last-aggregation-commit", cvToString(lastAggregationCommit));

  if (lastAggregationCommit.type === ClarityType.OptionalSome) {
    const blockHeightToCheck = lastAggregationCommit.value.value;
    let rewardSet = (await callReadOnlyFunction({
      contractAddress,
      contractName,
      functionName: "get-reward-set-at-block",
      functionArgs: [uintCV(cycleId), uintCV(blockHeightToCheck)],
      network,
      senderAddress: contractAddress,
    })) as SomeCV<TupleCV>;
    console.log("total-ustx from reward-set-at-block", blockHeightToCheck);
    console.log(
      cycleId,
      Number(
        (rewardSet.value.data["total-ustx"] as UIntCV).value.valueOf()
      ).toLocaleString()
    );

    const poxAddrIndex = (await callReadOnlyFunction({
      contractAddress,
      contractName,
      functionName: "get-pox-addr-index",
      functionArgs: [uintCV(cycleId)],
      network,
      senderAddress: contractAddress,
    })) as SomeCV<UIntCV>;

    console.log("total-ustx pox-3")
    console.log("poxAddrIndex", cvToString(poxAddrIndex));
    rewardSet = (await callReadOnlyFunction({
      contractAddress: poxContractAddress,
      contractName: "pox-3",
      functionName: "get-reward-set-pox-address",
      functionArgs: [uintCV(cycleId), poxAddrIndex.value],
      network,
      senderAddress: contractAddress,
    })) as SomeCV<TupleCV>;
    console.log(
      cycleId,
      Number(
        (rewardSet.value.data["total-ustx"] as UIntCV).value.valueOf() /
          1_000_000n
      ).toLocaleString()
    );
  } else {
    console.log("nothing");
  }
})();
