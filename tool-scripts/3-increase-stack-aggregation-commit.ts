import {
  AnchorMode,
  bufferCV,
  bufferCVFromString,
  makeContractCall,
  SignedContractCallOptions,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { handleTransaction, network, NEXT_CYCLE } from "../src/deploy";
import { boomboxes, poxContractAddress } from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";
import { hexToBytes } from "@stacks/common";

const { poolAdmin } = keys;


const rewardPoxAddrCV = tupleCV({
  hashbytes: bufferCV(hexToBytes("828095be0dffcd9aaab31ae291748b820e4d1158")),
  version: bufferCV(hexToBytes("04")),
});
/*poxAddrCVFromBitcoin(
  "tb1qs2qft0sdllxe424nrt3fzaytsg8y6y2cplaylj"
  // retired "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);
*/

const pools = [
  { admin: poolAdmin, nonce: 0 },
  
];

(async () => {
  const cycleId = NEXT_CYCLE; // <--- edit here
  for (let p of pools) {
    const admin = p.admin;
    const options: SignedContractCallOptions = {
      contractAddress: poxContractAddress,
      contractName: "pox-2",
      functionName: "stack-aggregation-increase",
      functionArgs: [rewardPoxAddrCV, uintCV(cycleId), uintCV(3)],
      senderKey: admin.private,
      network: network,
      fee: new BN(1000),
      anchorMode: AnchorMode.Any,
    };
    if (p.nonce > 0) {
      options.nonce = new BN(p.nonce);
    }
    const tx = await makeContractCall(options);

    await handleTransaction(tx);
  }
})();
