import {
  ClarityType,
  contractPrincipalCV,
  cvToString,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { STACKING_MINIMUM } from "../src/constants";
import {
  handleTransaction,
  mainnet,
  mocknet,
  NEXT_CYCLE,
  timeout,
} from "../src/deploy";
import {
  accountsApi,
  cycleLength,
  firstBurnChainBlock,
  infoApi,
  makePoxDelegateStackStxCall,
  transactionsApi,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  // retired "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);
const pools = [{ admin: poolAdmin, lockingPeriod: 1, nonce: 5184 }];

const cycleId = mainnet ? NEXT_CYCLE : 124;

async function delegateStack(
  pool: {
    admin: { stacks: string; private: string };
    lockingPeriod: number;
    nonce: number;
  },
  cycleId: number
) {
  // cycle info
  const info = await infoApi.getCoreApiInfo();
  //
  // start block of next cycle
  const startBlock = firstBurnChainBlock + cycleId * cycleLength - 100;
  const startBurnHeight = startBlock - 150;
  console.log({
    admin: pool.admin.stacks,
    nonce: pool.nonce,
    startBlock,
    burnBlockHeight: info.burn_block_height,
  });
  const length = 10;
  // Change here start
  const indices = undefined;
  const lockingPeriod = pool.lockingPeriod;
  const admin = pool.admin;
  // change here end

  let accountInfo = await accountsApi.getAccountInfo({
    principal: admin.stacks,
    proof: 0,
  });

  // overwrite nonce
  if (pool.nonce > 0) {
    accountInfo.nonce = pool.nonce;
  }
  console.log(accountInfo.nonce, pool.admin.stacks);

  const nonce = accountInfo.nonce;

  const stackerCV = contractPrincipalCV(
    "SP2HNY1HNF5X25VC7GZ3Y48JC4762AYFHKS061BM0",
    "stacking-contract"
  );
  const amount = uintCV(500000000);

  // find min amount
  const stxBalanceResponse = await accountsApi.getAccountStxBalance({
    principal: cvToString(stackerCV),
  });
  // only stack balance - 1 STX at most
  let balance = (stxBalanceResponse as any).balance;
  balance = balance > 1_000_000 ? balance - 1_000_000 : balance;

  const amountUstx = amount.value;
  const amountUstxCV = {
    type: ClarityType.UInt,
    // use minimum of delegated amount and balance - 1 STX
    value: amountUstx < balance ? amountUstx : balance,
  } as UIntCV;

  const tx = await makePoxDelegateStackStxCall({
    stackerCV,
    amountUstxCV,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    poolAdmin,
    nonce,
  });
  const process = nonce >= 0 && amountUstxCV.value >= STACKING_MINIMUM;
  console.log(
    `${cvToString(stackerCV)}, ${cvToString(
      amountUstxCV
    )}, ${nonce}, ${lockingPeriod}, ${(tx.txid(), process)}`
  );
  if (process) {
    let transaction: { tx_status: string } | undefined = undefined;
    const accountInfo = await accountsApi.getAccountInfo({
      principal: poolAdmin.stacks,
    });
    if (nonce >= accountInfo.nonce) {
      try {
        transaction = (await transactionsApi.getTransactionById({
          txId: tx.txid(),
          unanchored: true,
        })) as { tx_status: string };
      } catch {}
      if (!transaction || transaction.tx_status === "pending") {
        console.log({ txid: tx.txid() });
        try {
          await handleTransaction(tx);
        } catch (e: any) {
          if (e.toString().includes("ConflictingNonceInMempool")) {
            // ignore
            console.log("not replacing ", tx.txid(), {
              stacker: cvToString(stackerCV),
              nonce,
              poolAdmin: poolAdmin.stacks,
            });
          } else {
            console.log("failed to submit ", tx.txid(), {
              stacker: cvToString(stackerCV),
              nonce,
              poolAdmin: poolAdmin.stacks,
            });
            throw e;
          }
        }
        // wait for the stacks node to update the nonce
        if (mocknet) {
          await timeout(5000);
        } else {
          await timeout(30000);
        }
      }
    }
  }
}

(async () => {
  for (let p of pools) {
    delegateStack(p, cycleId).catch((e) => console.log(e));
  }
})();
