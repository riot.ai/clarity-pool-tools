import {
  callReadOnlyFunction,
  cvToHex,
  cvToString,
  hexToCV,
  uintCV,
} from "@stacks/transactions";
import { contractsApi } from "../src/pool-tool-utils";

(async () => {
  const auction = await callReadOnlyFunction({
    contractAddress: "SP2C2YFP12AJZB4MABJBAJ55XECVS7E4PMMZ89YZR",
    contractName: "arkadiko-auction-engine-v3-1",
    functionName: "get-auction-by-id",
    functionArgs: [uintCV(30)],

    senderAddress: "SP2C2YFP12AJZB4MABJBAJ55XECVS7E4PMMZ89YZR",
  });
  console.log(cvToString(auction));
})();
