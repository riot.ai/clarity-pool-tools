import { readFileSync } from "fs";
import { network, NEXT_CYCLE } from "../src/deploy";
import {
  downloadPoxTxs,
  writeDelegationStates,
  DelegationDataCV,
} from "../src/pool-tool-utils";
import { fastPool, keys } from "./config";

const { poolAdmin } = keys;

const pools = [{ admin: fastPool }, { admin: poolAdmin }];

async function writeDelegationAllStates() {
  const fromBlock = undefined; //76900; // <-- edit here
  const locked: any[] = [];
  const states: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[] = [];
  const txs = await downloadPoxTxs(network);

  for (let p of pools) {
    await writeDelegationStates(txs, p.admin.stacks, locked, states, fromBlock);
    console.log({ statesSoFar: states.length });
  }
}

(async () => {
  await writeDelegationAllStates();
})();
