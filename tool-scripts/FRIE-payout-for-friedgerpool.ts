import { stackerReplaceMap } from "../src/constants";
import { network, timeout } from "../src/deploy";
import { transferFRIE } from "../src/frie-tokens";
import {
  accountsApi,
  boomboxes,
  downloadBoomboxesTxs,
  downloadPoolPayoutHints,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  downloadPoxTxs,
  getPayoutHints,
  getStackersFromBoombox,
  getStackersFromPoolTool,
  getStackersFromPox,
  poolToolContract,
  poolToolContractV0,
  stackersToCycle,
  transactionsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, pool700, poolStxFoundation } =
  keys;

(async () => {
  const rewardCycle = 16;
  const rewardsBTC = 1.40052203 - 0.95; // 0.95; // 1.40052203 - 0.95;
  const exchangeRate = 3088; //3230; //3088;
  const rewardsBTC2 = 0.95; // 0.95; // 1.40052203 - 0.95;
  const exchangeRate2 = 3230; //3230; //3088;
  const totalRewards =
    (rewardsBTC * 100000000 * 1000000) / exchangeRate +
    (rewardsBTC2 * 100000000 * 1000000) / exchangeRate2;

  const memo = `reward cycle #${rewardCycle}`;

  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadPoxTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoolPayoutHints(network);
  const payoutHints = getPayoutHints();

  // get stackers from all cycles
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  console.log({ v0StackersLength: v0Stackers.length });
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    poolStxFoundation.stacks // exclude advocate pool members
  );
  console.log({ v1StackersLength: v1Stackers.length });
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ countBoomboxes: boomboxStackers.length });

  const poxStackers = getStackersFromPox(network, [
    poolAdmin,
    pool3cycles,
    pool6cycles,
    pool700,
  ]);

  console.log({ countPox: poxStackers.length });

  // filter stackers by cycle
  const stackersInCycle = stackersToCycle(
    v0Stackers.concat(v1Stackers).concat(boomboxStackers).concat(poxStackers),
    rewardCycle
  );

  const boomboxesInCycle = stackersToCycle(boomboxStackers, rewardCycle);
  const poxInCycle = stackersToCycle(poxStackers, rewardCycle);

  const stackers = stackersInCycle.members;
  const totalStacked = stackers.reduce((sum: number, s) => sum + s.amount, 0);
  const boomboxStacked = boomboxesInCycle.members.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  const poxStacked = poxInCycle.members.reduce(
    (sum: number, s) => sum + s.amount,
    0
  );
  const adjustedTotalRewards =
    totalRewards - (boomboxStacked * totalRewards) / totalStacked;
  const filteredExtendedStackers = stackers
    .filter(
      (s) =>
        boomboxesInCycle.members.findIndex((m) => m.stacker === s.stacker) < 0
    )
    .map((stacker) => {
      return {
        reward: Math.round((stacker.amount * totalRewards) / totalStacked),
        ...stacker,
      };
    });
  const totalPayout = filteredExtendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log({
    totalStacked,
    totalRewards,
    numberOfStackers: filteredExtendedStackers.length,
    adjustedTotalRewards,
    totalPayout,
    numberOfBoomboxers: boomboxesInCycle.members.length,
    boomboxPayout: (boomboxStacked * totalRewards) / totalStacked,
  });

  const events1 = (
    (await transactionsApi.getTransactionById({
      txId: "0x6985bcb817d4c7e468b7ea59a3bb60cbd3c996caa626cc80318da6dccd6b502e",
      eventLimit: 200,
      eventOffset: 0,
    })) as any
  ).events;

  const events2 = (
    (await transactionsApi.getTransactionById({
      txId: "0x6985bcb817d4c7e468b7ea59a3bb60cbd3c996caa626cc80318da6dccd6b502e",
      eventLimit: 200,
      eventOffset: 200,
    })) as any
  ).events;

  const filteredExtendedStackers2 = events1.concat(events2).map((e: any) => {
    return {
      rewardCycle: 16,
      reward: parseInt(e.asset.amount),
      stacker: e.asset.recipient,
    };
  });
  const results = await Promise.all([
    payoutByAdmin(filteredExtendedStackers2, poolAdmin, 1560, payoutHints),
  ]);
  console.log({ total: results.reduce((t, r) => (t += r.total), 0) });
})();

async function payoutByAdmin(
  stackers: {
    rewardCycle: number;
    amount: number;
    stacker: string;
    reward: number;
  }[],
  admin: { stacks: string; private: string },
  nonce: number,
  payoutHints: object
) {
  const accountInfo = await accountsApi.getAccountInfo({
    principal: admin.stacks,
  });
  const currentAccountNonce = accountInfo.nonce;
  let txCount = 0;
  let receiver;
  let total = 0.0;
  const memo = "Payout cycle #16";

  for (let s of stackers) {
    if (nonce + txCount === 1789) {
      txCount += 3;
    } else if (nonce + txCount === 1815) {
      txCount += 1;
    } else if (nonce + txCount === 1818) {
      txCount += 7;
    }
    if (nonce + txCount >= 1829) {
      return { total };
    }
    try {
      receiver = s.stacker;
      /*
      if (s.stacker in stackerReplaceMap) {
        receiver = (stackerReplaceMap as any)[s.stacker];
        console.log("replaced by mails", s.stacker, receiver);
      }
      if (s.stacker in payoutHints) {
        const stackerHint = (payoutHints as any)[s.stacker];
        if (stackerHint.recipient) {
          receiver = stackerHint.recipient;
          console.log("replaced by hints", s.stacker, receiver);
        }
      }
      */
      total += s.reward;
      if (nonce + txCount >= currentAccountNonce) {
        try {
          const tx = await transferFRIE(
            s.reward,
            receiver,
            admin,
            nonce + txCount,
            memo
          );
          console.log({ tx, nonce: nonce + txCount });
          await timeout(30000);
        } catch (e) {
          if (e.toString().includes("TooMuchChaining")) {
            await timeout(2400000);
            const tx = await transferFRIE(
              s.reward,
              receiver,
              admin,
              nonce + txCount,
              memo
            );
            console.log({ tx, nonce: nonce + txCount, retry: true });
          } else {
            console.log(e.toString());
            throw e;
          }
        }
      }
      txCount += 1;
    } catch (e) {
      if (e.toString().includes("BadNonce")) {
        // ignore
        txCount += 1;
      } else {
        console.log(e);
        throw e;
      }
    }
  }
  return { total };
}
