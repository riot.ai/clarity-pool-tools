import { writeFileSync } from "fs";
import { NEXT_CYCLE } from "../src/deploy";
import { stackersToCycle } from "../src/pool-tool-utils";
import { getStackedAmounts } from "./fast-pool-utils";

const cycle = NEXT_CYCLE; // <--- edit here

(async () => {
  const stackedAmounts = await getStackedAmounts(cycle);
  console.log(stackedAmounts)
  const stackerAddresses = Object.keys(stackedAmounts);

  let totalStacked = 0n;
  for (let stacker of stackerAddresses) {
    totalStacked = totalStacked + stackedAmounts[stacker].stackedAmount;
  }

  console.log(totalStacked, stackerAddresses.length);

  const stackers: {
    rewardCycle: number;
    lockingPeriod: number;
    amount: bigint;
    stacker: string;
    txid: string;
    timestamp: string;
    unlock: number;
    rewards?: number;
  }[] = [];

  for (let stacker of stackerAddresses) {
    const details = stackedAmounts[stacker];
    stackers.push({
      rewardCycle: cycle,
      lockingPeriod: 1,
      amount: details.stackedAmount,
      stacker: stacker,
      txid: details.txid,
      timestamp: details.timestamp,
      unlock: details.unlockHeight,
    });
  }

  console.log({stackers: stackers.length});
  // only logging
  const stackersInCycle = stackersToCycle(stackers, cycle);
  writeFileSync(`members-${cycle}.json`, JSON.stringify(stackersInCycle));
})();
