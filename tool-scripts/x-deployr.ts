import { StackingClient } from "@stacks/stacking";
import { makeContractDeploy } from "@stacks/transactions";
import { createSmartContractPayload } from "@stacks/transactions/dist/payload";
import { deployContract, network } from "../src/deploy";
import { contractsApi } from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin } = keys;

const deploy = async () => {
  const result = await deployContract(
    "pox-2-pool-admin-helper-2",
    undefined,
    undefined,
    poolAdmin.private,
    22
  );
  console.log(result);
};

deploy();
