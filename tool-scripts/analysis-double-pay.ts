import { cvToHex, standardPrincipalCV, tupleCV } from "@stacks/transactions";
import { readFileSync } from "fs";
import {
  accountsApi,
  contractsApi,
  poxContractAddress,
} from "../src/pool-tool-utils";

const doublePayRefunds32 = JSON.parse(
  readFileSync("double-pay-refund-32.json").toString()
);

const doublePayRefunds33 = JSON.parse(
  readFileSync("double-pay-refund-33.json").toString()
);

console.log(
  Object.keys(doublePayRefunds32).filter(
    (stacker: string) => doublePayRefunds33[stacker]
  )
);

console.log(
  Object.keys(doublePayRefunds33).filter(
    (stacker: string) => doublePayRefunds32[stacker]
  )
);

const total32 = Object.keys(doublePayRefunds32).reduce(
  (total: number, s: any) => total + doublePayRefunds32[s].doublePay,
  0
);
const total33 = Object.keys(doublePayRefunds33).reduce(
  (total: number, s: any) => total + doublePayRefunds33[s].doublePay,
  0
);
console.log(total32, total33);
