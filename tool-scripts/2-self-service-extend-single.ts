import { StacksMainnet } from "@stacks/network";
import {
  AnchorMode,
  PostConditionMode,
  broadcastTransaction,
  listCV,
  makeContractCall,
  principalCV,
} from "@stacks/transactions";
import { network } from "../src/deploy";
import { accountsApi } from "../src/pool-tool-utils";
import { fastPool, keys } from "./config";
import { getPendingPoolMembers } from "./fast-pool-utils";
import { bytesToHex } from "@stacks/common";
import { readFileSync } from "fs";
(async () => {
  const mainnet = new StacksMainnet();

  const { poolHelper } = keys;
  const [contractAddress, contractName] = fastPool.stacks.split(".");

  // define pending members and nonce
  //

  let pendingPoolMembers = [] as {
    stacker: string;
  }[];

  //console.log(pendingPoolMembers.length);

  if (pendingPoolMembers.length === 0) {
    //return;
  }
  pendingPoolMembers = [
    {
      stacker:
        "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-mia-mining-v2",
    },
    {
      stacker:
        "SP8A9HZ3PKST0S42VM9523Z9NV42SZ026V4K39WH.ccd002-treasury-nyc-mining-v2",
    },
  ];

  //
  // get nonce
  //
  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolHelper.stacks,
    proof: 0,
  });

  let nonce = accountInfo.nonce;

  console.log({ nonce }, poolHelper.stacks);

  // extend members one by one
  for (let i = 0; i <= pendingPoolMembers.length; i++) {
    let member = pendingPoolMembers[i];
    //console.log(batch.map((m) => m.stacker));
    const tx = await makeContractCall({
      contractAddress,
      contractName,
      functionName: "delegate-stack-stx",
      functionArgs: [principalCV(member.stacker)],
      network: mainnet,
      anchorMode: AnchorMode.Any,
      postConditionMode: PostConditionMode.Allow,
      fee: 750_001, // 22.5 STX
      nonce: nonce + i,
      senderKey: poolHelper.private,
    });

    //console.log(bytesToHex(tx.serialize()));
    //console.log("\n");
    const result = await broadcastTransaction(tx, network);
    console.log(result);
  }
})();
