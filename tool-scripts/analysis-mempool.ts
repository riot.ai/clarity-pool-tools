import { network, STACKS_API_URL } from "../src/deploy";
import {
  downloadMempoolTxs,
  logMempoolTxsCSV,
  transactionsApi,
  txAsString,
} from "../src/pool-tool-utils";
import { Block, StacksApiSocketClient } from "@stacks/blockchain-api-client";
import { writeFileSync } from "fs";

async function handleMempool() {
  const suffix = new Date().toISOString();
  await downloadMempoolTxs(suffix, network);
  logMempoolTxsCSV(suffix, network);
}

(async () => {
  await handleMempool();
  const connection = StacksApiSocketClient.connect({
    url: STACKS_API_URL,
  });
  const sub = connection.subscribeBlocks();
  connection.socket.on("block", async (block) => {
    console.log(block);
    const blockStrings = [];
    for (let txId of block.txs) {
      const tx = await transactionsApi.getTransactionById({ txId });
      blockStrings.push(txAsString(tx));
    }
    writeFileSync(`block-${block.height}`, blockStrings.join("\n"));
    await handleMempool();
  });
  const sub1 = connection.subscribeMempool();
  connection.socket.on("mempool", async (tx) => {
    console.log(tx.fee_rate, tx.nonce);
  });
})();
