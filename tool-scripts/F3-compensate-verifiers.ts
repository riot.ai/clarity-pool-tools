import {
  AnchorMode,
  bufferCV,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import BN from "bn.js";
import { handleTransaction, network } from "../src/deploy";
import { accountsApi, getPayoutHints } from "../src/pool-tool-utils";
import { keys } from "./config";
const { poolAdmin } = keys;

const dryrun = false;

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce: number,
  memo: string,
  payoutHints: object
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          return tupleCV({
            to: standardPrincipalCV(receiver),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from(memo)),
          });
        })
      ),
    ],
    senderKey: poolAdmin.private,
    nonce: nonce ? new BN(nonce) : undefined,
    network: network,
    postConditions: [
      makeStandardSTXPostCondition(
        poolAdmin.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const result = await handleTransaction(tx);
    console.log({ result });
  }
}

async function payVerifiers() {
  const verifiers = [
    {
      stacker: "SPWE4ZR7WHPG80DZ49RBPWMYK9Q8VCJW6PV8XDNQ",
      reward: 1940_000_000,
    },
    {
      stacker: "SP82S7H3DPXG6NN2YGW413DSK5Q83BT59E92G1H1",
      reward: 10_204_081,
    },
    {
      stacker: "SP1JSH2FPE8BWNTP228YZ1AZZ0HE0064PS6RXRAY4",
      reward: 10204081,
    },
    {
      stacker: "SP8R8AT0HS12MRKHMZ22J05DMTBDHRW9XJBKE5DG",
      reward: 193877551,
    },
    {
      stacker: "SP1GRRA4P0VHKPRF3MKNX9XD5CPBTCYQE6P1JZ2W9",
      reward: 20408163,
    },
    {
      stacker: "SPAPG0NQHMYN495GSTGN74H8BBD3DMSNYHZGT3P5",
      reward: 10204081,
    },
    {
      stacker: "SPX272JTYGP48WAFNGSQKZWKGAJ1T1CT2D74HSSE",
      reward: 20408163,
    },
    {
      stacker: "SP23XS735PDV3A6W7156QAZYZ7A978F641K8PAER4",
      reward: 10204081,
    },
    {
      stacker: "SP779SC9CDWQVMTRXT0HZCEHSDBXCHNGG7BC1H9B",
      reward: 204081632,
    },
    {
      stacker: "SP1TXGE9FX8JG75QXGADJKSDQFENTVAMJ5JM6YD5G",
      reward: 20408163,
    },
  ];
  const totalPayoutSet = verifiers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log({ totalPayoutSet });

  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let nonce = 445; //accountInfo.nonce;

  payoutSet(verifiers, 2439999996, nonce, "BTC tx verifier <3", await getPayoutHints());
}

(async () => {
  await payVerifiers();
})();
