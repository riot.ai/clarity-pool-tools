//
// utils functions

import {
  AnchorMode,
  broadcastRawTransaction,
  broadcastTransaction,
  makeContractDeploy,
  makeSTXTokenTransfer,
  StacksTransaction,
  TxBroadcastResultOk,
  TxBroadcastResultRejected,
} from "@stacks/transactions";
import { StacksMainnet, StacksTestnet } from "@stacks/network";
import * as fs from "fs";
const BN = require("bn.js");
const fetch = require("node-fetch");

import { ADDR1, ADDR2, ADDR3, ADDR4, testnetKeyMap } from "./mocknet";

export const local = true;
export const mocknet = false;
export const noSidecar = false;
export const mainnet = true;
export const NEXT_CYCLE = mainnet ? 82 : 6;

const STACKS_CORE_API_URL = local
  ? noSidecar
    ? "http://localhost:20443"
    : "http://localhost:3999"
  : mainnet
  ? //"https://proportionate-holy-knowledge.stacks-mainnet.discover.quiknode.pro/057f49d7f2148311cb0b0f12dfc9a751c02dba05"
    "https://api.mainnet.hiro.so"
  : //"https://stacks-node.planbetter.org"
    "https://2-1-api.testnet.hiro.so";

export const STACKS_API_URL = local
  ? "http://localhost:3999"
  : mainnet
  ? //"https://stacks-node.planbetter.org"
    "https://api.hiro.so"
  : //"https://proportionate-holy-knowledge.stacks-mainnet.discover.quiknode.pro/057f49d7f2148311cb0b0f12dfc9a751c02dba05"
    "https://2-1-api.testnet.hiro.so";
export const network = mainnet
  ? new StacksMainnet({ url: STACKS_CORE_API_URL })
  : new StacksTestnet({ url: STACKS_CORE_API_URL });

export const user = mainnet
  ? JSON.parse(
      fs
        .readFileSync(
          "../../../../Documents/_private_backyard/blockstack/miner-keys.json"
        )
        .toString()
    )
  : JSON.parse(
      fs
        .readFileSync(
          "../../../github/blockstack/stacks-blockchain/keychain2.json"
        )
        .toString()
    );

export async function handleTransaction(transaction: StacksTransaction) {
  try {
    const rawTx = transaction.serialize();
    const result = await broadcastRawTransaction(
      rawTx,
      network.getBroadcastApiUrl(),
      undefined,
      async (url: string, init?: any) => {
        const response = await fetch(url, init);
        return response;
      }
    );
    console.log({
      txId: result,
      timestamp: new Date().toLocaleString(),
      nonce: transaction.auth.spendingCondition?.nonce,
    });
    if ((result as TxBroadcastResultRejected).error) {
      if (
        (result as TxBroadcastResultRejected).reason === "ContractAlreadyExists"
      ) {
        console.log("already deployed");
        return {} as TxBroadcastResultOk;
      } else if ((result as TxBroadcastResultRejected).reason === "BadNonce") {
        console.log("already broadcasted");
        return {} as TxBroadcastResultOk;
      } else {
        console.log(result);
        throw new Error(
          `failed to handle transaction ${transaction.txid()}: ${JSON.stringify(
            result
          )}`
        );
      }
    }
    const processed = await processing((result as TxBroadcastResultOk).txid);
    if (!processed) {
      throw new Error(
        `failed to process transaction ${transaction.txid()}: transaction not found`
      );
    }
    console.log({ txId: result, processed });
    return result as TxBroadcastResultOk;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export async function deployContract(
  contractName: string,
  path: string = `./contracts/${contractName}.clar`,
  changeCode: (s: string) => string = (s) => s,
  userPrivate: string = user.private,
  nonce: number | undefined = undefined
) {
  const codeBody = fs.readFileSync(path).toString();
  console.log(codeBody);
  var transaction = await makeContractDeploy({
    contractName,
    codeBody: changeCode(codeBody),
    senderKey: userPrivate,
    network,
    anchorMode: AnchorMode.Any,
    fee: 200_000,
    nonce,
  });
  console.log(`deploy contract ${contractName}`);
  return handleTransaction(transaction);
}

export async function faucetCall(recipient: string, amount: number) {
  console.log("init wallet");
  const transaction = await makeSTXTokenTransfer({
    recipient,
    amount: new BN(amount),
    senderKey: testnetKeyMap[ADDR3].private,
    network,
    anchorMode: AnchorMode.Any,
  });
  return handleTransaction(transaction);
}

export function timeout(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function processing(txId: string, count: number = 0): Promise<boolean> {
  return true || noSidecar
    ? processingWithoutSidecar(txId, count)
    : processingWithSidecar(txId, count);
}

async function processingWithoutSidecar(
  tx: string,
  count: number = 0
): Promise<boolean> {
  await timeout(1000);
  return true;
}

async function processingWithSidecar(
  txId: string,
  count: number = 0
): Promise<boolean> {
  const url = `${STACKS_API_URL}/extended/v1/tx/${txId}`;
  var result = await fetch(url);
  var value = await result.json();
  console.log(count);
  if (value.tx_status === "success") {
    console.log(`transaction ${txId} processed`);
    console.log(value);
    return false;
  }
  if (value.tx_status === "pending") {
    console.log(value);
  } else if (count === 10) {
    console.log(value);
  }

  if (count > 60) {
    console.log(`failed after ${count} trials`);
    console.log(value);
    return false;
  }

  if (mocknet) {
    await timeout(5000);
  } else {
    await timeout(120000);
  }
  return processing(txId, count + 1);
}
