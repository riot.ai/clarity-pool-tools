import {
  AnchorMode,
  contractPrincipalCV,
  cvToString,
  hexToCV,
  makeContractCall,
} from "@stacks/transactions";
import { readFileSync, writeFileSync } from "fs";
import { getTxId, concatTransaction } from "./btcTransactions";
import { FPWR_04_CONTRACT, FPWR_04_DEPOT_CONTRACT } from "./constants";
import { handleTransaction, network } from "./deploy";
import BN from "bn.js";

export const filenameByStxTxId = "fpwr-tx-stx.json";
export const filenameByBtcTxId = "fpwr-tx-btc.json";

async function loadStxTxIds() {
  try {
    const content = readFileSync(filenameByStxTxId).toString();
    return JSON.parse(content);
  } catch (e) {
    console.log(e);
    return {};
  }
}

async function loadBtcTxIds() {
  try {
    const content = readFileSync(filenameByBtcTxId).toString();
    return JSON.parse(content);
  } catch (e) {
    console.log(e);
    return {};
  }
}

export async function getFPBtcTxs() {
  const txsFilePath = `acc-txs-${network.chainId}-SP1JSH2FPE8BWNTP228YZ1AZZ0HE0064PS6RXRAY4.fpwr-v03.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());
  txs = txs.filter(
    (t: any) =>
      t.tx_status === "success" &&
      t.tx_type === "contract_call" &&
      t.contract_call.function_name === "mint"
  );
  const stxTxIds = await loadStxTxIds();
  const btcTxIds = await loadBtcTxIds();

  for (let tx of txs) {
    if (!stxTxIds[tx.tx_id]) {
      const height = (
        hexToCV(tx.contract_call.function_args[0].hex) as any
      ).data.height.value.toString(10);
      const txPartsCV = hexToCV(tx.contract_call.function_args[1].hex);
      const txBuff = await concatTransaction(txPartsCV);
      const btcTxIdCV = await getTxId(txBuff);
      const btcTxId = cvToString(btcTxIdCV);
      stxTxIds[tx.tx_id] = btcTxId;
      btcTxIds[btcTxId] = tx.tx_id;
    } else {
      btcTxIds[stxTxIds[tx.tx_id]] = tx.tx_id;
    }
  }

  writeFileSync(filenameByStxTxId, JSON.stringify(stxTxIds));
  writeFileSync(filenameByBtcTxId, JSON.stringify(btcTxIds));

  return { txs, btcTxIds, stxTxIds };
}

export async function updateRewardAdmin(
  admin: { private: string },
  nonce: number
) {
  const tx = await makeContractCall({
    contractAddress: FPWR_04_CONTRACT.address,
    contractName: FPWR_04_CONTRACT.name,
    functionName: "update-reward-admin",
    functionArgs: [
      contractPrincipalCV(
        FPWR_04_DEPOT_CONTRACT.address,
        FPWR_04_DEPOT_CONTRACT.name
      ),
    ],
    senderKey: admin.private,
    network: network,
    postConditions: [],
    nonce: nonce ? new BN(nonce) : undefined,
    anchorMode: AnchorMode.Any,
  });

  const result = await handleTransaction(tx);
  console.log({ result });
}
