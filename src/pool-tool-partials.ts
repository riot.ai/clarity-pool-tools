import {
  Configuration,
  InfoApi,
  SmartContractsApi,
} from "@stacks/blockchain-api-client";
import { StackingClient } from "@stacks/stacking";
import {
  ClarityType,
  ClarityValue,
  contractPrincipalCV,
  cvToHex,
  hexToCV,
  noneCV,
  SomeCV,
  standardPrincipalCV,
  TupleCV,
  tupleCV,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { network, STACKS_API_URL } from "./deploy";
import { accountsApi, poxContractAddress } from "./pool-tool-utils";
import { poxAddrCVFromBitcoin } from "./utils-pox-addr";
const fetch = require("node-fetch");

const config = new Configuration({
  basePath: STACKS_API_URL,
  fetchApi: fetch,
});
export const infoApi = new InfoApi(config);
const contractsApi = new SmartContractsApi(
  new Configuration({
    basePath: STACKS_API_URL,
    fetchApi: fetch,
  })
);

export async function getPartialStacked(
  poolAdmin: string,
  rewardCycle: number,
  btcAddressCV: ClarityValue
) {
  const [part1, part2] = poolAdmin.split(".");
  const key = cvToHex(
    tupleCV({
      "pox-addr": btcAddressCV,
      "reward-cycle": uintCV(rewardCycle),
      sender: part2
        ? contractPrincipalCV(part1, part2)
        : standardPrincipalCV(poolAdmin),
    })
  );
  try {
    const amountHex = await contractsApi.getContractDataMapEntry({
      contractAddress: poxContractAddress,
      contractName: "pox-2",
      mapName: "partial-stacked-by-cycle",
      key,
      proof: 0,
    });
    const amountCV = hexToCV(amountHex.data);
    if (amountCV.type === ClarityType.OptionalNone) {
      return { amount: "none", cycle: rewardCycle, poolAdmin, btcAddressCV };
    } else {
      return {
        amount: (
          ((amountCV as SomeCV).value as TupleCV).data[
            "stacked-amount"
          ] as UIntCV
        ).value.toString(10),
        cycle: rewardCycle,
        poolAdmin,
        btcAddressCV,
      };
    }
  } catch (e) {
    console.log(e);
  }
}

export async function getDelegatedUsers(
  startingBlock: number,
  endingBlock: number,
  pages: number = 20
): Promise<Array<any>> {
  const apiConfig = new Configuration({
    fetchApi: fetch,
    basePath: network.coreApiUrl,
  });

  var userList: Array<any> = [];
  var limit = 50;
  var skippedRepeat = 0;

  for (var i = 0; i < pages; i++) {
    const txPage = await accountsApi.getAccountTransactions({
      principal: "SPXVRSEH2BKSXAEJ00F1BY562P45D5ERPSKR4Q33.bitcoin-brink-pool",
      offset: i * limit,
      limit,
    });
    if (txPage.results.length <= 0) {
      break;
    }

    txPage.results.forEach((tx: any) => {
      const senderAddress = tx["sender_address"];
      const blockHeight = tx["block_height"];
      if (
        blockHeight >= startingBlock &&
        blockHeight <= endingBlock &&
        tx["tx_result"].hex === "0x0703"
      ) {
        if (tx["tx_type"] === "contract_call") {
          const callTx = tx["contract_call"];
          if (callTx["function_name"] === "delegate-stx") {
            const amountHex = callTx["function_args"][0]["hex"];
            const amountCV = hexToCV(amountHex) as UIntCV;

            var exists = false;
            console.log(amountCV.value.toString(), senderAddress);
            userList.forEach((user) => {
              if (user.address === senderAddress) {
                skippedRepeat = skippedRepeat + 1;
                exists = true;
              }
            });

            if (!exists) {
              userList.push({
                address: senderAddress,
                amount: amountCV.value.toString(),
              });
            }
          }
        }
      }
    });
  }

  return userList;
}
