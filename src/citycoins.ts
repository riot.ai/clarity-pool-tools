import { hexToCV, ListCV, UIntCV } from "@stacks/transactions";
import { readFileSync } from "fs";
import { network } from "./deploy";
import { downloadAccountTxs } from "./pool-tool-utils";

const miaCore = "SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-core-v2";
const miaToken = "SP466FNC0P7JWTNM2R9T199QRZN1MYEDTAR0KP27.miamicoin-token-v2";
export async function downloadMiamicoinTxs() {
  await downloadAccountTxs(miaCore, network);
  await downloadAccountTxs(miaToken, network);
}
export function logCitycoinCSV() {
  const txsFilePath = `acc-txs-${network.chainId}-${miaCore}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  let winners = JSON.parse(readFileSync("mia-winners-v2.json").toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(
    Array.of(
      "time",
      "block",
      "function",
      "sender",
      "fees",
      "nonce",
      "winner",
      "amount",
      "number-of-blocks or cycles",
      "block-height"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "stack-tokens":
        details = [
          Number(
            (hexToCV(tx.contract_call.function_args[0].hex) as UIntCV).value
          ),
          Number(
            (hexToCV(tx.contract_call.function_args[1].hex) as UIntCV).value
          ),
        ];
        break;
      case "claim-mining-reward":
        details = [
          250000,
          "",
          tx.contract_call.function_args[0].repr, // block height
        ];
        break;
      case "mine-tokens":
        details = [
          Number(
            (hexToCV(tx.contract_call.function_args[0].hex) as UIntCV).value
          ),
          1,
        ];
        break;
      case "mine-many":
        details = [
          tx.post_conditions[0].amount,
          (hexToCV(tx.contract_call.function_args[0].hex) as ListCV).list
            .length,
        ];
        break;
      default:
        details = [];
    }
    console.log(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.sender_address,
        tx.fee_rate,
        tx.nonce,
        winners[tx.block_height]?.miner || "",
        ...details
      ).join(",")
    );
  }
}

export function logCitycoinTokenCSV() {
  const txsFilePath = `acc-txs-${network.chainId}-${miaToken}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(
    Array.of(
      "time",
      "block",
      "function",
      "sender",
      "amount",
      "number-of-blocks or cycles",
      "block-height"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "transfer":
        details = [
          Number(
            (hexToCV(tx.contract_call.function_args[0].hex) as UIntCV).value
          ),
          tx.contract_call.function_args[2].repr,
        ];
        break;
      default:
        details = [];
    }
    console.log(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
}
