
;; helper function to revoke and delegate in one transaction
(define-public (revoke-and-delegate-stx (amount-ustx uint)
                                        (delegate-to principal)
                                        (until-burn-ht (optional uint))
                                        (pox-addr (optional {version: (buff 1), hashbytes: (buff 32)})))
  (begin
    (try! 
      (contract-call? 'ST000000000000000000002AMW42H.pox-2 revoke-delegate-stx))
    (contract-call? 'ST000000000000000000002AMW42H.pox-2 delegate-stx amount-ustx delegate-to until-burn-ht pox-addr)))
