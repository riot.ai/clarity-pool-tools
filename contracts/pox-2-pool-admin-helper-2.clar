
;; helper function to increase locking amount and 
;; to extend locking period in one transaction
(define-public (extend-and-increase (stacker principal)
                                    (pox-addr {version: (buff 1), hashbytes: (buff 32)})                                    
                                    (extend-count uint)
                                    (increase-by uint))
  (begin
    (try!       
      (contract-call? 'ST000000000000000000002AMW42H.pox-2 delegate-stack-extend stacker pox-addr extend-count))
    (contract-call? 'ST000000000000000000002AMW42H.pox-2 delegate-stack-increase stacker pox-addr increase-by)))
